import Vue from 'vue'
import App from './App.vue'
import 'jquery'
import jQuery from 'popper.js';

window.$ = window.jQuery = jQuery;

import 'bootstrap'
import './assets/css/app.scss'
import {router} from './router';
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
