import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './assets/views/Home.vue'
import Article from './assets/views/Article.vue'
Vue.use(VueRouter)

const routes = [
    { path: '/', component: Home },
    { path: '/article/:id', component: Article },
  ]
  
// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
export const router = new VueRouter({
    routes // short for `routes: routes`
})